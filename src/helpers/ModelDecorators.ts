import "reflect-metadata"
const EXPOSED_PARAMS_FOR: string[][] = [];

export enum EExposureTypes {
    OrderBy,
    With,
}

//Decorator Factory
export function ExposeFor(a: EExposureTypes): Function {
    if(!Array.isArray(EXPOSED_PARAMS_FOR[a])) {
        EXPOSED_PARAMS_FOR[a] = [];
    }

    // the original decorator
    function actualDecorator(target: Object, property: string): void {
        EXPOSED_PARAMS_FOR[a].push(property);
    }

    // return the decorator
    return actualDecorator;
}

export function GetExposedParamsFor(a: EExposureTypes): string[] {
    return EXPOSED_PARAMS_FOR[a];
}