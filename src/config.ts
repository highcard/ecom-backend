import convict from "convict";

const config = convict({
    env: {
        doc: "Application environment",
        format: ["production", "development", "test"],
        default: "development",
        env: "NODE_ENV"
    },
    express: {
        doc: "All configuration concerning express.",
        port: {
            doc: "Webserver port",
            format: "port",
            default: 3000
        }
    },
    db: {
        doc: "Database credentials",
        host: {
            doc: "Database hostname/ip",
            format: '*', //mabe write custom format to validate hostname/ip?
            default: "localhost"
        },
        port: {
            doc: "Database port",
            format: "port",
            default: 3306
        },
        username: {
            doc: "Database username",
            format: "*",
            default: "root",
        },
        password: {
            doc: "Database password",
            format: "*",
            default: "root"
        },
        synchronize: {
            doc: "Indicates if database schema should be auto created on every application launch.",
            format: Boolean,
            default: true
        },
    }
});

config.loadFile(`./config/${config.get('env')}.json`);
config.validate({allowed: 'strict'});

export default config;