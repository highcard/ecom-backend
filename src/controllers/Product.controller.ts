import {
    Controller, Get, Render, Post, 
    Authenticated, Required, BodyParams,
    Delete,
    PathParams,
    QueryParams
  } from "@tsed/common";

import { ProductService } from "../services/Product.service";
import { Product } from "../models/Product.model";
import { NotFound } from "ts-httpexceptions";
import { ProductImageController } from "./ProductImage.controller";



@Controller("/products", ProductImageController)
export class ProductController {
    constructor(private productService: ProductService) {

    }
    @Get("/")
    async getAll(
        @QueryParams("offset") offset: number,
        @QueryParams("limit") limit: number,
        @QueryParams("orderBy") orderBy: string,
        @QueryParams("order") order: string
    ): Promise<Product[]> {
        return this.productService.all(offset, limit, orderBy, order);
    }

    @Get("/:id")
    async get(
        @Required() @PathParams("id") id: number,
        @QueryParams("with") sub: string[]
    ): Promise<Product> {
        const product: Product = await this.productService.one(id, sub);
        if(!product) {
            throw new NotFound(`Product not found.`);
        }
        return product;
    }
}