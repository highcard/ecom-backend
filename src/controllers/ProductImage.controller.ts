import {
    Controller, Get, Render, Post, 
    Authenticated, Required, BodyParams,
    Delete,
    PathParams,
    QueryParams,
    MergeParams
  } from "@tsed/common";


import { ProductImage } from "../models/ProductImage.model";
import { NotFound } from "ts-httpexceptions";
import { ProductImageService } from "../services/ProductImage.service";



@Controller("/:productId/images")
@MergeParams(true)
export class ProductImageController {
    constructor(private productImageService: ProductImageService) {

    }
    @Get("/")
    async getAll(
        @Required() @PathParams("productId") productId: number
    ): Promise<ProductImage[]> {
        return this.productImageService.allOf(productId);
    }
}