import {ServerLoader, ServerSettings} from "@tsed/common";
import "@tsed/typeorm";
import "@tsed/ajv";

//ANCHOR use template literals for all outputs.

@ServerSettings({
    rootDir: __dirname,
    acceptMimes: ["application/json"],
    mount: {
        "/api": "${rootDir}/controllers/**/*.ts"
    },
    typeorm: [
        {
            "name": "default",
            "type": "mysql",
            "host": "localhost",
            "port": 8889,
            "username": "root",
            "password": "root",
            "database": "ecom",
            "synchronize": true,
            "logging": false,
            "entities": [
               "src/models/**/*.ts"
            ],
            "migrations": [
               "src/migrations/**/*.ts"
            ],
            "subscribers": [
               "src/subscribers/**/*.ts"
            ]
         }
    ]
})
export class Server extends ServerLoader {
    public $onMountingMiddlewares(): void {
        //configure middlewares
    }

    public $onReady(): void {
        console.log("Server is live!");
    }

    public $onServerInitError(err: any) {
        console.error(err);
    }
}

new Server().start();