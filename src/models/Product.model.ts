import {Property, MaxLength, Required} from "@tsed/common";
import {Entity, PrimaryGeneratedColumn, Column, Index, 
    CreateDateColumn, UpdateDateColumn, OneToMany} from "typeorm"
import { ProductImage } from "./ProductImage.model";

import {ExposeFor, EExposureTypes} from "../helpers/ModelDecorators"

@Entity({name: "products"})
export class Product {

    @PrimaryGeneratedColumn()
    //
    @ExposeFor(EExposureTypes.OrderBy)
    id: number;
    
    @OneToMany(type => ProductImage, image => image.product)
    //
    @ExposeFor(EExposureTypes.With)
    images: ProductImage[];

    @Index()
    @Column()
    //
    @ExposeFor(EExposureTypes.OrderBy)
    name: string;

    @Column()
    description: string;

    @Column()
    price: number;

    @CreateDateColumn()
    //
    @ExposeFor(EExposureTypes.OrderBy)
    createdAt: Date;

    @UpdateDateColumn()
    //
    @ExposeFor(EExposureTypes.OrderBy)
    updatedAt: Date;

    @Column()
    isAvailable: boolean;
}
