import {Property, MaxLength, Required} from "@tsed/common";
import {Entity, PrimaryGeneratedColumn, Column, Index, 
    CreateDateColumn, UpdateDateColumn, ManyToOne, JoinColumn} from "typeorm"

import {Product} from "./Product.model"

@Entity({name: "product_images"})
export class ProductImage {
    @PrimaryGeneratedColumn()
    id: number;
    
    @ManyToOne(type => Product, product => product.images)
    @Index()
    @JoinColumn()
    product: Product;

    @Column()
    url: string;

    @CreateDateColumn()
    createdAt: Date;

    @UpdateDateColumn()
    updatedAt: Date;
}
