import {Service} from "@tsed/common";
import {TypeORMService} from "@tsed/typeorm";
import {Connection} from "typeorm";
import { Product } from "../models/Product.model";
import { GetExposedParamsFor, EExposureTypes } from "../helpers/ModelDecorators";

@Service()
export class ProductService {
    private connection: Connection;

    constructor(private typeORMService: TypeORMService) {
    }

    $afterRoutesInit() {
        this.connection = this.typeORMService.get();
    }

    async all(offset: number = 0, limit: number = 250, orderBy?: string, order?: string): Promise<Product[]> {
        const query = this.connection.manager
        .getRepository(Product)
        .createQueryBuilder("product")
        .leftJoinAndSelect("product.images", "image")
        .skip(offset)
        .take(limit)

        if(GetExposedParamsFor(EExposureTypes.OrderBy).includes(orderBy)) {
            query.orderBy(`product.${orderBy}`, (order === "DESC") ? order : "ASC");
        }

        return query.getMany();
    }

    async one(id: number, sub: string[]): Promise<Product> {
        if(!sub) {
            sub = [];
        }
        sub = GetExposedParamsFor(EExposureTypes.With).filter(item => sub.includes(item));

        return this.connection.manager.getRepository(Product).findOne(id, { relations: sub});
    }
}