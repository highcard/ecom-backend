import {Service} from "@tsed/common";
import {TypeORMService} from "@tsed/typeorm";
import {Connection, OrderByCondition} from "typeorm";
import { ProductImage } from "../models/ProductImage.model";
import { Product } from "../models/Product.model";
import { NotFound } from "ts-httpexceptions";
import { ProductService } from "./Product.service";

//try to automatically generate the custom keys
//@see: https://github.com/kimamula/ts-transformer-keys
const VALID_ORDERBY_PARAMS = ["id", "name",  "createdAt", "updatedAt"];
const VALID_SUB_PARAMS = ["images"];

@Service()
export class ProductImageService {
    private connection: Connection;

    constructor(private typeORMService: TypeORMService,
        private productService: ProductService) {
    }

    $afterRoutesInit() {
        this.connection = this.typeORMService.get();
    }

    async allOf(productId: number): Promise<ProductImage[]> {
        const product: Product = await this.productService.one(productId, ["images"]);

        if(!product) {
            throw new NotFound("Product not found");
        }

        return product.images;
    }
}